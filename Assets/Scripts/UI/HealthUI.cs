﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthUI : MonoBehaviour
{
    public List<GameObject> hearts;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateUI(int life)
    {
        Debug.Log("Le joueur a " + life + " coeurs");
        for(int h = 0; h < hearts.Count; h++)
        {
            if(h < life)
            {
                hearts[h].SetActive(true);
            }
            else
            {
                hearts[h].SetActive(false);
            }
        }
    }
}
