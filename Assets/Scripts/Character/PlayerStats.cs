﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    public HealthUI HealthUI;
    public int maxHealth = 3;
    public int currentHealth;
    
    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        HealthUI.UpdateUI(maxHealth);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GainHealth()
    {
        if(currentHealth < maxHealth)
        {
            currentHealth+=1;
            HealthUI.UpdateUI(currentHealth);
        }
    }

    public void LoseHealth(int damage)
    {
        if(currentHealth > 0)
        {
            currentHealth -= damage;
            HealthUI.UpdateUI(currentHealth);
        }
        else
        {
            //Game Over
        }

    }
}
