﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionsHandler : MonoBehaviour
{
    public PlayerStats stats;
    void Start()
    {
        stats = GetComponent<PlayerStats>();
    }

    void Update()
    {

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Lorsqu'on entre en collision
        if (collision.gameObject.CompareTag("Enemy"))
        {
            stats.LoseHealth(1);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("HealthItem"))
        {
            stats.GainHealth();
        }
    }
}
