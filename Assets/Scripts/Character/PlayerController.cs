﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public bool isWalking = false;
    public float speed = 5f;
    public float jumpForce = 5f;

    public float velocityThreshold = 0.1f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            //Tourner le sprite vers la droite (donc ne pas l'inverser)
            GetComponent<SpriteRenderer>().flipX = false;
            //Aller vers la droite
            transform.position += new Vector3(speed * Time.deltaTime, 0, 0);
            //Si elle ne marche pas et est sur une plateforme
            if (!isWalking && IsGrounded())
            {
                //On la fait marcher
                isWalking = true;
                GetComponent<Animator>().SetBool("walking", true);
            }
        }
        else if (Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.LeftArrow))
        {
            //Tourner le sprite vers la gauche (donc l'inverser)
            GetComponent<SpriteRenderer>().flipX = true;
            //Aller vers la gauche
            transform.position += new Vector3(-speed * Time.deltaTime, 0, 0);
            //Si elle ne marche pas et est sur une plateforme
            if (!isWalking && IsGrounded())
            {
                //On la fait marcher
                isWalking = true;
                GetComponent<Animator>().SetBool("walking", true);
            }
        }
        //Si on n'appuie pas sur une touche pour la faire marcher
        else
        {
            //Si elle était en train de marcher jusaue là et qu'elle est sur une plateforme 
            if (isWalking && IsGrounded())
            {
                //On arrete de la faire marcher
                isWalking = false;
                GetComponent<Animator>().SetBool("walking", false);
            }
        }
        //Si on appuie sur espace et qu'elle est sur une plateforme
        if (Input.GetKeyDown(KeyCode.Space) && IsGrounded())
        {
            //On ajoute de la vélocité en y, on arrete la marche et on déclenche le saut
            GetComponent<Rigidbody2D>().velocity = new Vector3(0, jumpForce, 0);
            isWalking = false;
            GetComponent<Animator>().SetBool("walking", false);
            GetComponent<Animator>().SetTrigger("jump");
        }
    }

    //IsGrounded donne un résultat booléen :
    //true si
    //-velocityThreshold < GetComponent<Rigidbody2D>().velocity.y < velocityThreshold
    //false sinon
    bool IsGrounded() {
        return Mathf.Abs(GetComponent<Rigidbody2D>().velocity.y) < velocityThreshold;
    }
}
